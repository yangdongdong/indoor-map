# indoor-map
基于`three.js`实现室内地图（半成品都没算...）

# 参考（demo文件夹内）
## 道易寻
[广东省第二人民医院](https://map.ipsmap.com/?socketKey=1535546007754&bluetooth=ok#/map?socketKey=1535546007754&bluetooth=ok&id=X8KBQ92KxP)

## 蜂鸟云
[在线体验](https://www.fengmap.com/fmAPI/help-fmdemo.html#FMSearchAnalysisBaseInfo)

## wolfwind521（indoor3D-master）
[indoor3D](https://github.com/wolfwind521/indoor3D)

## WoShiSunWuKong（Indoor-Map-master）
[Indoor-Map](https://github.com/WoShiSunWuKong/Indoor-Map)
